## Création d'une pipeline d'intégration continue pour des images Docker

#####  Pour créer les pipelines, nous avons eu besoin premierement d'un fichier Dockerfile et de 3 stages. 
#### 
####    **Stage Lint**
######      Effectue un lint avec hadolint pour déterminer si la syntaxe du fichier Dockerfile est correcte.**
######      N'autorise pas "fail"
####
####   **Stage Build**
######      Effectue un Build de votre image à partir du fichier Dockerfile présent dans votre dépôt.**
######      Attention à utiliser une image docker dans le runner
####
####   **Stage Scan**
######      Effectue un scan de vulnérabilités de votre image docker grâce à différents outils existants 
######      Utilisation de l'outil Trivy pour effectuer le Scan
####

#### **Contenu du Dockerfile**
######      Le Dockerfile créant l'image Docker à un service Ansible fonctionnel.
######      L'image Docker créée est basée sur une image Debian Jessie(Debian 8)
